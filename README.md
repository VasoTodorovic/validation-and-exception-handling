
# Validation and exception handling
- validation path parametar, request querry, request body with default and custom validation
- exception handling of validation to send 400 status code insted of 500 and custom message
- testing if respond status is 400 and contains custom message



## @Valid

 - [@Valid-configure response  by application properties to send message](https://gitlab.com/VasoTodorovic/validation-and-exception-handling/-/blob/master/src/main/resources/application.properties)
 - [DTO](https://gitlab.com/VasoTodorovic/validation-and-exception-handling/-/tree/master/src/main/java/com/example/validation1/entity)
 - [MethodArgumentNotValidException](https://reflectoring.io/bean-validation-with-spring-boot/)

## @Validate
- [@Validate -configure response through controlleradvice](https://gitlab.com/VasoTodorovic/validation-and-exception-handling/-/blob/master/src/main/java/com/example/validation1/exception/HotelControlerAdvice.java)
- [path parametar querry variabla](https://www.google.com/search?q=validate+path+parameter&oq=validate+path+parametar+&aqs=chrome.1.69i57j0i22i30j0i5i13i30l6.7632j0j7&sourceid=chrome&ie=UTF-8)
- [ConstraintViolationException](https://medium.com/javarevisited/are-you-using-valid-and-validated-annotations-wrong-b4a35ac1bca4)
 
## Sonarqube

- https://sonarcloud.io/summary/overall?id=VasoTodorovic_validation-and-exception-handling
## Appendix

Validation 

https://medium.com/javarevisited/are-you-using-valid-and-validated-annotations-wrong-b4a35ac1bca4
https://github.com/zzpzaf/customvalidation1/blob/main/src/main/resources/application.properties#L4

Testing

https://www.codingeek.com/forum/672/string-response-body-mockmvc
https://www.briansdevblog.com/2017/05/rest-endpoint-testing-with-mockmvc/