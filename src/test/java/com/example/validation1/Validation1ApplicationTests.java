package com.example.validation1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.RequestEntity.post;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class Validation1ApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    //same as getByStars() one way
    public void getByID() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/-1"))
                .andReturn();
        String s = result.getResponse().getContentAsString();
        assertEquals(result.getResponse().getStatus(), 400);
        assertThat(result.getResponse().getContentAsString()).contains("number must be positive");
    }
    @Test
    // second way
    public void getByStars() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/").param("stars", "6")).andReturn();
        String expectedJson = "{\"status\":\"BAD_REQUEST\",\"message\":\"getByStars.stars: The Stars number must be 1,2,3,4 or 5\",\"errors\":[\"com.example.validation1.controller.HotelController getByStars.stars: The Stars number must be 1,2,3,4 or 5\"]}";
        this.mockMvc.perform(get("/").param("stars", "6"))
                .andExpect(status().is4xxClientError())
                .andReturn();

        String actualJson = result.getResponse().getContentAsString();

        assertEquals(expectedJson, actualJson);
    }

    @Test
    public void create() throws Exception {
        this.mockMvc.perform(get("/-1").contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":1,\n" +
                                "\"name\":\"Hilton\",\n" +
                                "\"stars\":6\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }


}



