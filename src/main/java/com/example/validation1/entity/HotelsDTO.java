package com.example.validation1.entity;

import com.example.validation1.validators.OneOfIntegers;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class HotelsDTO {

    @NotNull(message = "The item Name is required!")
    private Long id;
    @Size(min = 1, max = 100, message
            = "name must be between 1 and 100 characters")
    public  String name;
    @OneOfIntegers
    private Integer stars;

}
