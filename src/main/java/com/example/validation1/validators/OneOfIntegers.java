package com.example.validation1.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target( { ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = OneOfIntegersValidator.class)
public  @interface OneOfIntegers {
    public String message() default "The Stars number must be 1,2,3,4 or 5";
    //represents group of constraints
    public Class<?>[] groups() default {};
    //The Group attribute, which we can define in case the annotation belongs to a group of annotations. The default should be an empty array of type Class<?>
    public Class<? extends Payload>[] payload() default {};
//he Payload attribute can be used for defining custom payload objects for our constraint. The default should also be an empty array of type Class<?>
}
