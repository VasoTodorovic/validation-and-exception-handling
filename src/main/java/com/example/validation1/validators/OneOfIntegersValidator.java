package com.example.validation1.validators;

import com.example.validation1.validators.OneOfIntegers;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class OneOfIntegersValidator implements ConstraintValidator<OneOfIntegers, Integer> {

    Integer[] arrayOfValues;


    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        arrayOfValues = new Integer[] {1, 2, 3,4,5};
        return  Arrays.asList(this.arrayOfValues).contains(value);
    }

}