package com.example.validation1.controller;

import com.example.validation1.entity.HotelsDTO;
import com.example.validation1.validators.OneOfIntegers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;


@RestController
@Validated
public class HotelController {

    @GetMapping("/{id}")
    public ResponseEntity getByID(  @Positive(message = "number must be positive") @PathVariable   Long id) {
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity getByStars(  @OneOfIntegers @RequestParam  Integer stars) {
        return new ResponseEntity(stars, HttpStatus.OK);
    }




    @PostMapping("/validation")
    public ResponseEntity create(  @RequestBody @Valid HotelsDTO hotelsDTO) {

        return new ResponseEntity(hotelsDTO, HttpStatus.OK);
    }
}
